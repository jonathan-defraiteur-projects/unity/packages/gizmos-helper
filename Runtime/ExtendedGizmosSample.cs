using JonathanDefraiteur.Helpers;
using UnityEngine;

namespace JonathanDefraiteur.Sample
{
    public class ExtendedGizmosSample : MonoBehaviour
    {
        public enum Display
        {
            Filled,
            Wired,
            Both,
        }
        
        public bool autoRotation = false;
        
        [Header("DrawCube")]
        public Color cubeColor = Color.magenta;
        public Display cubeDisplay = Display.Both;
        public Vector3 cubePosition = Vector3.zero;
        public Vector3 cubeRotation = new Vector3(45, 45, 45);
        public Vector3 cubeScale = new Vector3(1, 3, 2);
        
        [Header("DrawShpere")]
        public Color sphereColor = Color.cyan;
        public Display sphereDisplay = Display.Both;
        public Vector3 spherePosition = Vector3.zero;
        public Vector3 sphereRotation = new Vector3(45, 45, 45);
        public Vector3 sphereScale =  new Vector3(1, 3, 2);

        private void OnDrawGizmos()
        {
            if (autoRotation) {
                cubeRotation.Set(
                    cubeRotation.x + 10f * Time.deltaTime,
                    cubeRotation.y + 20f * Time.deltaTime,
                    cubeRotation.z + 30f * Time.deltaTime
                    );
                sphereRotation.Set(
                    sphereRotation.x + 10f * Time.deltaTime,
                    sphereRotation.y + 20f * Time.deltaTime,
                    sphereRotation.z + 30f * Time.deltaTime
                    );
            }
            
            // Cube
            if (cubeDisplay == Display.Filled || cubeDisplay == Display.Both) {
                ExtendedGizmos.DrawCube(cubePosition, cubeScale, Quaternion.Euler(cubeRotation), cubeColor);
            }
            if (cubeDisplay == Display.Wired || cubeDisplay == Display.Both) {
                ExtendedGizmos.DrawWireCube(cubePosition, cubeScale, Quaternion.Euler(cubeRotation), cubeColor);
            }

            // Sphere
            if (sphereDisplay == Display.Filled || sphereDisplay == Display.Both) {
                ExtendedGizmos.DrawSphere(spherePosition, sphereScale, Quaternion.Euler(sphereRotation), sphereColor);
            }
            if (sphereDisplay == Display.Wired || sphereDisplay == Display.Both) {
                ExtendedGizmos.DrawWireSphere(spherePosition, sphereScale, Quaternion.Euler(sphereRotation), sphereColor);
            }
            
        }
    }
}