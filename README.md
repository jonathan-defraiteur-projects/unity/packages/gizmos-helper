# To Install

## Via scoped registries
Setup your scoped registry,
then add `com.jonathan-defraiteur.unity.gizmos-helper` to your dependencies:

```
# ./Packages/manifest.json
{
  "dependencies": {
    "com.jonathan-defraiteur.unity.gizmos-helper": "latest"
    ...
  },
  "scopedRegistries": [
    {
      "name": "NPM JS Registry",
      "url": "https://registry.npmjs.org/",
      "scopes": [
        "com.jonathan-defraiteur"
      ]
    }
  ],
}
```

## Via gitlab url
Add the following to your dependencies:
```
# ./Packages/manifest.json
{
  "dependencies": {
    "com.jonathan-defraiteur.unity.gizmos-helper": "https://gitlab.com/jonathan-defraiteur/unity/gizmos-helper.git",
    ...
  }
}
```
